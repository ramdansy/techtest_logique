import 'package:flutter/material.dart';

class AppColors {
  static const colorSchema = ColorScheme(
    brightness: Brightness.light,
    onPrimary: cPrimary,
    primary: cPrimary,
    onSecondary: cSecondary,
    secondary: cSecondary,
    error: cAccentError,
    onError: cAccentError,
    background: cGrey10,
    onBackground: cGrey10,
    surface: cBgLight,
    onSurface: cBgLight,
  );

  static const Color cPrimary = cPrimary60;

  //secondary
  static const Color cSecondary = Color(0xFFFFAD58);
  static const Color cSecondary5 = Color(0xFFFFF1E1);

  //background
  static const Color cBgWhite = cTextLight;
  static const Color cBgLight = cGrey20;
  static const Color cBgDark = Color(0xFF2C3941);
  static const Color cBgSoftDark = Color(0xFF115E38);
  static const Color cBgSoftDark2 = Color(0xFF1A7E4C);
  static const Color cBgSoftDark3 = Color(0xFF1F8F58);
  static const Color cPurple = Color(0xFFC3CCEB);

  //text
  static const Color cTextDark = Color(0xFF030E08);
  static const Color cTextLight = Color(0xFFFFFFFF);

  //primary
  static const Color cPrimary70 = Color(0xFF2c82a8);
  static const Color cPrimary60 = Color(0xFF3695ba);
  static const Color cPrimary50 = Color(0xFF3ea2c7);
  static const Color cPrimary40 = Color(0xFF4eafcd);
  static const Color cPrimary30 = Color(0xFF64bdd4);
  static const Color cPrimary20 = Color(0xFF89cfdf);
  static const Color cPrimary10 = Color(0xFFb5e2eb);
  static const Color cPrimary5 = Color(0xFFe2f4f7);

  //greyscale
  static const Color cGrey100 = Color.fromARGB(255, 12, 52, 28);
  static const Color cGrey80 = Color(0xFF515458);
  static const Color cGrey60 = Color(0xFF8C8F93);
  static const Color cGrey40Disable = Color(0xFFC8CCD2);
  static const Color cGrey20 = Color(0xFFF2F4F6);
  static const Color cGrey10 = Color(0xFFFFFFFF);

  //accent
  static const Color cAccentSuccess = Color(0xFF00AB66);
  static const Color cAccentError = Color(0xFFE11900);
  static const Color cAccentInformation = Color(0xFF33B5E7);
  static const Color cAccentWarning = Color(0xFFf0ad4e);
}
