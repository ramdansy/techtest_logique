import 'package:dartz/dartz.dart';

import '../../../utils/failure.dart';
import '../../entities/base_data.dart';
import '../../entities/post_preview.dart';

abstract class PostRepository {
  Future<Either<Failure, BaseData<PostPreview>>> getListPostByUserId(
      String userId, int page, int limit);
  Future<Either<Failure, BaseData<PostPreview>>> getListPost(
      int page, int limit);
}
