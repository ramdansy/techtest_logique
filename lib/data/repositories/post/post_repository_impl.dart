import 'dart:io';

import 'package:dartz/dartz.dart';

import '../../../app_constant.dart';
import '../../../domain/models/post_preview_model.dart';
import '../../../utils/exceptions.dart';
import '../../../utils/failure.dart';
import '../../datasource/remote/post_remote_data_source.dart';
import '../../entities/base_data.dart';
import '../../entities/post_preview.dart';
import 'post_repository.dart';

class PostRepositoryImpl implements PostRepository {
  final PostRemoteDataSource remoteDataSource;

  PostRepositoryImpl({required this.remoteDataSource});

  @override
  Future<Either<Failure, BaseData<PostPreview>>> getListPostByUserId(
      String userId, int page, int limit) async {
    try {
      final response =
          await remoteDataSource.getListPostByUserId(userId, page, limit);
      final convert = parseListPostToEntity(response);

      return Right(convert);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.toString()));
    } on SocketException {
      return const Left(ConnectionFailure(AppConstant.textErrorConnection));
    }
  }

  @override
  Future<Either<Failure, BaseData<PostPreview>>> getListPost(
      int page, int limit) async {
    try {
      final response = await remoteDataSource.getListPost(page, limit);
      final convert = parseListPostToEntity(response);

      return Right(convert);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.toString()));
    } on SocketException {
      return const Left(ConnectionFailure(AppConstant.textErrorConnection));
    }
  }
}
