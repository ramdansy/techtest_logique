import 'dart:io';

import 'package:dartz/dartz.dart';

import '../../../app_constant.dart';
import '../../../domain/models/user_preview_model.dart';
import '../../../utils/exceptions.dart';
import '../../../utils/failure.dart';
import '../../datasource/remote/user_remote_data_source.dart';
import '../../entities/base_data.dart';
import '../../entities/user.dart';
import '../../entities/user_preview.dart';
import 'user_repository.dart';

class UserRepositoryImpl implements UserRepository {
  final UserRemoteDataSource remoteDataSource;

  UserRepositoryImpl({required this.remoteDataSource});

  @override
  Future<Either<Failure, BaseData<UserPreview>>> getListUser(
      int page, int limit) async {
    final response = await remoteDataSource.getListUser(page, limit);
    final convert = parseListUserToEntity(response);
    return Right(convert);
  }

  @override
  Future<Either<Failure, User>> getuserById(String id) async {
    try {
      final response = await remoteDataSource.getUserById(id);
      return Right(response.toEntity());
    } on ServerException catch (e) {
      return Left(ServerFailure(e.toString()));
    } on SocketException {
      return const Left(ConnectionFailure(AppConstant.textErrorConnection));
    }
  }
}
