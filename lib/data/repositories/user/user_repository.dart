import 'package:dartz/dartz.dart';

import '../../../utils/failure.dart';
import '../../entities/base_data.dart';
import '../../entities/user.dart';
import '../../entities/user_preview.dart';

abstract class UserRepository {
  Future<Either<Failure, BaseData<UserPreview>>> getListUser(
      int page, int limit);
  Future<Either<Failure, User>> getuserById(String id);
}
