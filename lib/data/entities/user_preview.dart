class UserPreview {
  String? id;
  String? title;
  String? firstName;
  String? lastName;
  String? picture;

  UserPreview({
    required this.id,
    required this.title,
    required this.firstName,
    required this.lastName,
    required this.picture,
  });
}
