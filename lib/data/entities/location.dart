class Location {
  String? street;
  String? city;
  String? state;
  String? country;
  String? timezone;

  Location({
    required this.street,
    required this.city,
    required this.state,
    required this.country,
    required this.timezone,
  });
}
