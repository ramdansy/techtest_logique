import '../../domain/models/location_model.dart';

class User {
  String? id;
  String? title;
  String? firstName;
  String? lastName;
  String? gender;
  String? email;
  String? dateOfBirth;
  String? registerDate;
  String? phone;
  String? picture;
  LocationModel? location;

  User({
    required this.id,
    required this.title,
    required this.firstName,
    required this.lastName,
    required this.gender,
    required this.email,
    required this.dateOfBirth,
    required this.registerDate,
    required this.phone,
    required this.picture,
    required this.location,
  });
}
