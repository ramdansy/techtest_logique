import 'package:techtest_logique/domain/models/user_preview_model.dart';

class PostPreview {
  String? id;
  String? text;
  String? image;
  int? likes;
  List<dynamic>? tags;
  String? publishDate;
  UserPreviewModel? owner;

  PostPreview({
    required this.id,
    required this.text,
    required this.image,
    required this.likes,
    required this.tags,
    required this.publishDate,
    required this.owner,
  });
}
