class BaseData<T> {
  int? total;
  int? page;
  int? limit;
  List<T>? data;

  BaseData({
    required this.total,
    required this.page,
    required this.limit,
    required this.data,
  });
}
