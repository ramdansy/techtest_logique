import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:techtest_logique/domain/models/user_model.dart';

import '../../../app_constant.dart';
import '../../../domain/models/base_data_model.dart';
import '../../../domain/models/user_preview_model.dart';

abstract class UserRemoteDataSource {
  Future<BaseDataModel<UserPreviewModel>> getListUser(int page, int limit);
  Future<UserModel> getUserById(String id);
}

class UserRemoteDataSourceImpl implements UserRemoteDataSource {
  final Dio remote;
  UserRemoteDataSourceImpl({required this.remote});

  @override
  Future<BaseDataModel<UserPreviewModel>> getListUser(
      int page, int limit) async {
    remote.options.headers["app-id"] = AppConstant.appId;

    try {
      final response = await remote
          .get("${AppConstant.baseUrl}/user?page=$page&limit=$limit");
      return compute(parseListUser, response.data);
    } catch (e) {
      throw Exception('Failed: $e');
    }
  }

  @override
  Future<UserModel> getUserById(String id) async {
    remote.options.headers["app-id"] = AppConstant.appId;

    try {
      final response = await remote.get("${AppConstant.baseUrl}/user/$id");
      return compute(userModelFromJson, response.data);
    } catch (e) {
      throw Exception('Failed: $e');
    }
  }
}
