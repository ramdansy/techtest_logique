import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../../../app_constant.dart';
import '../../../domain/models/base_data_model.dart';
import '../../../domain/models/post_preview_model.dart';

abstract class PostRemoteDataSource {
  Future<BaseDataModel<PostPreviewModel>> getListPostByUserId(
      String userId, int page, int limit);
  Future<BaseDataModel<PostPreviewModel>> getListPost(int page, int limit);
}

class PostRemoteDataSourceImpl implements PostRemoteDataSource {
  final Dio remote;
  PostRemoteDataSourceImpl({required this.remote});

  @override
  Future<BaseDataModel<PostPreviewModel>> getListPostByUserId(
      String userId, int page, int limit) async {
    remote.options.headers["app-id"] = AppConstant.appId;

    try {
      final response = await remote.get(
          "${AppConstant.baseUrl}/user/$userId/post?page=$page&limit=$limit");
      return compute(parseListPost, response.data);
    } catch (e) {
      throw Exception('Failed: $e');
    }
  }

  @override
  Future<BaseDataModel<PostPreviewModel>> getListPost(
      int page, int limit) async {
    remote.options.headers["app-id"] = AppConstant.appId;

    try {
      final response = await remote
          .get("${AppConstant.baseUrl}/post?page=$page&limit=$limit");
      return compute(parseListPost, response.data);
    } catch (e) {
      throw Exception('Failed: $e');
    }
  }
}
