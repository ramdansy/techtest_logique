import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'app/routes/app_pages.dart';
import 'app_binding.dart';
import 'app_colors.dart';
import 'app_text.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  initializeDateFormatting("id_ID", null).then((_) {
    runApp(
      GetMaterialApp(
        title: "Application",
        initialRoute: AppPages.INITIAL,
        initialBinding: AppBinding(),
        getPages: AppPages.routes,
        debugShowCheckedModeBanner: false,
        theme: ThemeData.light().copyWith(
          colorScheme: AppColors.colorSchema,
          textTheme: AppText.textTheme,
        ),
      ),
    );
  });
}
