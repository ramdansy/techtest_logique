import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:techtest_logique/data/datasource/remote/post_remote_data_source.dart';
import 'package:techtest_logique/data/datasource/remote/user_remote_data_source.dart';
import 'package:techtest_logique/data/repositories/post/post_repository.dart';
import 'package:techtest_logique/data/repositories/post/post_repository_impl.dart';
import 'package:techtest_logique/data/repositories/user/user_repositories_impl.dart';
import 'package:techtest_logique/data/repositories/user/user_repository.dart';
import 'package:techtest_logique/domain/usecases/post/get_all_post.dart';
import 'package:techtest_logique/domain/usecases/post/get_post_by_yser_id.dart';
import 'package:techtest_logique/domain/usecases/users/get_list_user.dart';
import 'package:techtest_logique/domain/usecases/users/get_user_by_id.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(Dio());

    /// Services
    _apiServices();

    /// Repositores
    _repositories();

    /// Usecases
    _useCases();
  }

  void _apiServices() {
    Get.lazyPut<UserRemoteDataSource>(
        () => UserRemoteDataSourceImpl(remote: Get.find()));
    Get.lazyPut<PostRemoteDataSource>(
        () => PostRemoteDataSourceImpl(remote: Get.find()));
  }

  void _repositories() {
    Get.put<UserRepository>(UserRepositoryImpl(remoteDataSource: Get.find()));
    Get.put<PostRepository>(PostRepositoryImpl(remoteDataSource: Get.find()));
  }

  void _useCases() {
    Get.put(GetListUser(Get.find()));
    Get.put(GetUserById(Get.find()));
    Get.put(GetPostByUserId(Get.find()));
    Get.put(GetAllPost(Get.find()));
  }
}
