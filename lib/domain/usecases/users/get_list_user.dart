import 'package:dartz/dartz.dart';
import 'package:techtest_logique/data/entities/base_data.dart';
import 'package:techtest_logique/data/entities/user_preview.dart';
import 'package:techtest_logique/data/repositories/user/user_repository.dart';
import 'package:techtest_logique/utils/failure.dart';

class GetListUser {
  final UserRepository repository;

  GetListUser(this.repository);

  Future<Either<Failure, BaseData<UserPreview>>> execute(int page, int limit) {
    return repository.getListUser(page, limit);
  }
}
