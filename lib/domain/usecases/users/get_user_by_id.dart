import 'package:dartz/dartz.dart';

import '../../../data/entities/user.dart';
import '../../../data/repositories/user/user_repository.dart';
import '../../../utils/failure.dart';

class GetUserById {
  final UserRepository repository;

  GetUserById(this.repository);

  Future<Either<Failure, User>> execute(String id) {
    return repository.getuserById(id);
  }
}
