import 'package:dartz/dartz.dart';

import '../../../data/entities/base_data.dart';
import '../../../data/entities/post_preview.dart';
import '../../../data/repositories/post/post_repository.dart';
import '../../../utils/failure.dart';

class GetPostByUserId {
  final PostRepository repository;

  GetPostByUserId(this.repository);

  Future<Either<Failure, BaseData<PostPreview>>> execute(
      String userId, int page, int limit) {
    return repository.getListPostByUserId(userId, page, limit);
  }
}
