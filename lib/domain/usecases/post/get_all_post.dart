import 'package:dartz/dartz.dart';

import '../../../data/entities/base_data.dart';
import '../../../data/entities/post_preview.dart';
import '../../../data/repositories/post/post_repository.dart';
import '../../../utils/failure.dart';

class GetAllPost {
  final PostRepository repository;

  GetAllPost(this.repository);

  Future<Either<Failure, BaseData<PostPreview>>> execute(int page, int limit) {
    return repository.getListPost(page, limit);
  }
}
