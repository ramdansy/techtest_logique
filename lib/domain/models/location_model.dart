import 'dart:convert';

import '../../data/entities/location.dart';

LocationModel userModelFromJson(String str) =>
    LocationModel.fromJson(json.decode(str));

String userModelToJson(LocationModel data) => json.encode(data.toJson());

class LocationModel {
  String street;
  String city;
  String state;
  String country;
  String timezone;

  LocationModel({
    required this.street,
    required this.city,
    required this.state,
    required this.country,
    required this.timezone,
  });

  factory LocationModel.fromJson(Map<String, dynamic> json) => LocationModel(
        street: json["street"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        timezone: json["timezone"],
      );

  Map<String, dynamic> toJson() => {
        "street": street,
        "city": city,
        "state": state,
        "country": country,
        "timezone": timezone,
      };

  Location toEntity() => Location(
        street: street,
        city: city,
        state: state,
        country: country,
        timezone: timezone,
      );
}
