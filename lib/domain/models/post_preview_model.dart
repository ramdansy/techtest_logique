import 'dart:convert';

import '../../data/entities/base_data.dart';
import '../../data/entities/post_preview.dart';
import 'base_data_model.dart';
import 'user_preview_model.dart';

PostPreviewModel postPreviewModelFromJson(dynamic str) =>
    PostPreviewModel.fromJson(str);

String postPreviewModelToJson(PostPreviewModel data) =>
    json.encode(data.toJson());

BaseDataModel<PostPreviewModel> parseListPost(dynamic response) {
  return BaseDataModel<PostPreviewModel>.fromJson(
      response, PostPreviewModel.fromJson);
}

BaseData<PostPreview> parseListPostToEntity(
    BaseDataModel<PostPreviewModel> response) {
  return BaseData<PostPreview>(
    total: response.total,
    page: response.page,
    limit: response.limit,
    data: response.data.map((e) => e.toEntity()).toList(),
  );
}

class PostPreviewModel {
  String id;
  String text;
  String image;
  int likes;
  List<dynamic> tags;
  String publishDate;
  UserPreviewModel owner;

  PostPreviewModel({
    required this.id,
    required this.text,
    required this.image,
    required this.likes,
    required this.tags,
    required this.publishDate,
    required this.owner,
  });

  factory PostPreviewModel.fromJson(Map<String, dynamic> json) =>
      PostPreviewModel(
        id: json["id"],
        text: json["text"],
        image: json["image"],
        likes: json["likes"],
        tags: json["tags"],
        publishDate: json["publishDate"],
        owner: UserPreviewModel.fromJson(json["owner"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "text": text,
        "image": image,
        "likes": likes,
        "tags": tags,
        "publishDate": publishDate,
        "owner": owner,
      };

  PostPreview toEntity() => PostPreview(
        id: id,
        text: text,
        image: image,
        likes: likes,
        tags: tags,
        publishDate: publishDate,
        owner: owner,
      );
}
