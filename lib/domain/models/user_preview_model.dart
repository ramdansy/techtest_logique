import 'dart:convert';

import 'package:techtest_logique/data/entities/base_data.dart';
import 'package:techtest_logique/data/entities/user_preview.dart';
import 'package:techtest_logique/domain/models/base_data_model.dart';

UserPreviewModel userPreviewModelFromJson(String str) =>
    UserPreviewModel.fromJson(json.decode(str));

String userPreviewModelToJson(UserPreviewModel data) =>
    json.encode(data.toJson());

BaseDataModel<UserPreviewModel> parseListUser(dynamic response) {
  return BaseDataModel<UserPreviewModel>.fromJson(
      response, UserPreviewModel.fromJson);
}

BaseData<UserPreview> parseListUserToEntity(
    BaseDataModel<UserPreviewModel> response) {
  return BaseData<UserPreview>(
    total: response.total,
    page: response.page,
    limit: response.limit,
    data: response.data.map((e) => e.toEntity()).toList(),
  );
}

class UserPreviewModel {
  String id;
  String title;
  String firstName;
  String lastName;
  String picture;

  UserPreviewModel({
    required this.id,
    required this.title,
    required this.firstName,
    required this.lastName,
    required this.picture,
  });

  factory UserPreviewModel.fromJson(Map<String, dynamic> json) =>
      UserPreviewModel(
        id: json["id"],
        title: json["title"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        picture: json["picture"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "firstName": firstName,
        "lastName": lastName,
        "picture": picture,
      };

  UserPreview toEntity() => UserPreview(
        id: id,
        title: title,
        firstName: firstName,
        lastName: lastName,
        picture: picture,
      );
}
