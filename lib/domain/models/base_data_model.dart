import 'package:techtest_logique/data/entities/base_data.dart';

class BaseDataModel<T> {
  final int total;
  final int page;
  final int limit;
  final List<T> data;

  BaseDataModel({
    required this.total,
    required this.page,
    required this.limit,
    required this.data,
  });

  factory BaseDataModel.fromJson(Map<String, dynamic> json, Function fromJson) {
    List<T> list = [];

    if (json["data"] != null) {
      if (json["data"].toString()[0] != "{") {
        list = List<T>.from(
          json["data"].map((x) => fromJson(x)),
        );
      } else {
        list.add(fromJson(json["data"]));
      }
    }

    return BaseDataModel(
      total: json["total"],
      page: json["page"],
      limit: json["limit"],
      data: list,
    );
  }

  BaseData<T> toEntity() => BaseData<T>(
        total: total,
        page: page,
        limit: limit,
        data: data,
      );
}
