import 'dart:convert';

import '../../data/entities/user.dart';
import 'location_model.dart';

UserModel userModelFromJson(dynamic str) => UserModel.fromJson(str);

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  String id;
  String title;
  String firstName;
  String lastName;
  String gender;
  String email;
  String dateOfBirth;
  String registerDate;
  String phone;
  String picture;
  LocationModel location;

  UserModel({
    required this.id,
    required this.title,
    required this.firstName,
    required this.lastName,
    required this.gender,
    required this.email,
    required this.dateOfBirth,
    required this.registerDate,
    required this.phone,
    required this.picture,
    required this.location,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["id"],
        title: json["title"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        gender: json["gender"],
        email: json["email"],
        dateOfBirth: json["dateOfBirth"],
        registerDate: json["registerDate"],
        phone: json["phone"],
        picture: json["picture"],
        location: LocationModel.fromJson(json["location"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "firstName": firstName,
        "lastName": lastName,
        "gender": gender,
        "email": email,
        "dateOfBirth": dateOfBirth,
        "registerDate": registerDate,
        "phone": phone,
        "picture": picture,
        "location": location,
      };

  User toEntity() => User(
        id: id,
        title: title,
        firstName: firstName,
        lastName: lastName,
        gender: gender,
        email: email,
        dateOfBirth: dateOfBirth,
        registerDate: registerDate,
        phone: phone,
        picture: picture,
        location: location,
      );
}
