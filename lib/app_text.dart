import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'app_colors.dart';

class AppText {
  static final textTheme = TextTheme(bodyLarge: fBodyLarge);

  //heading
  static final TextStyle fHeadingBig =
      GoogleFonts.inter(fontSize: 55, color: AppColors.cTextDark);
  static final TextStyle fHeading1 =
      GoogleFonts.inter(fontSize: 34, color: AppColors.cTextDark);
  static final TextStyle fHeading2 =
      GoogleFonts.inter(fontSize: 28, color: AppColors.cTextDark);
  static final TextStyle fHeading3 =
      GoogleFonts.inter(fontSize: 24, color: AppColors.cTextDark);
  static final TextStyle fHeading4 =
      GoogleFonts.inter(fontSize: 22, color: AppColors.cTextDark);
  static final TextStyle fHeading5 =
      GoogleFonts.inter(fontSize: 20, color: AppColors.cTextDark);
  static final TextStyle fHeading6 =
      GoogleFonts.inter(fontSize: 18, color: AppColors.cTextDark);

  //body
  static final TextStyle fBodyLarge =
      GoogleFonts.inter(fontSize: 16, color: AppColors.cTextDark);
  static final TextStyle fBodySmall =
      GoogleFonts.inter(fontSize: 14, color: AppColors.cTextDark);

  //caption
  static final TextStyle fCaptionLarge =
      GoogleFonts.inter(fontSize: 12, color: AppColors.cTextDark);
  static final TextStyle fCaptionSmall =
      GoogleFonts.inter(fontSize: 10, color: AppColors.cTextDark);
}
