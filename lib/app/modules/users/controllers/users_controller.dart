import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:techtest_logique/data/entities/user_preview.dart';

import '../../../../domain/usecases/users/get_list_user.dart';

class UsersController extends GetxController {
  late final GetListUser getListUser;

  UsersController(this.getListUser);

  ScrollController scrollController = ScrollController();
  RxBool isFirstLoading = false.obs,
      isLastdata = false.obs,
      isMoreDataLoading = false.obs;
  RxInt page = 0.obs;
  int limit = 20;
  List<UserPreview> listUser = <UserPreview>[].obs;

  @override
  void onInit() {
    super.onInit();
    scrollController.addListener(detectScrolledToEnd);
  }

  @override
  void onReady() {
    super.onReady();
    fetchListUser();
  }

  detectScrolledToEnd() async {
    if (scrollController.position.pixels ==
        scrollController.position.maxScrollExtent) {
      if (!isLastdata.value && !isMoreDataLoading.value) {
        await fetchListUser();
        // isMoreDataLoading.value = true;
      }
    }
  }

  Future<void> fetchListUser() async {
    isFirstLoading.value = true;

    final resListUser = await getListUser.execute(page.value, limit);
    resListUser.fold(
      (failure) => Fluttertoast.showToast(msg: failure.message),
      (data) {
        if ((data.data ?? []).length < 20) {
          isLastdata.value = true;
        }
        listUser.addAll(data.data ?? []);
      },
    );

    page.value++;
    isFirstLoading.value = false;
    // isMoreDataLoading.value = false;
  }
}
