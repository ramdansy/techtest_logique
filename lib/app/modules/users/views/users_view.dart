import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skeletons/skeletons.dart';
import 'package:techtest_logique/app/modules/user_detail/views/user_detail_view.dart';

import '../../../../app_colors.dart';
import '../../../../app_constant.dart';
import '../../../../app_text.dart';
import '../controllers/users_controller.dart';

class UsersView extends GetView<UsersController> {
  static const pageName = '/users';
  const UsersView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: AppColors.cBgWhite,
          width: Get.width,
          height: Get.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(AppConstant.paddingNormal),
                child: Text(
                  AppConstant.textUsers,
                  style:
                      AppText.fHeading3.copyWith(fontWeight: FontWeight.bold),
                ),
              ),
              Expanded(
                child: Obx(
                  () {
                    return RefreshIndicator(
                      onRefresh: () => controller.fetchListUser(),
                      child: GridView.builder(
                        controller: controller.scrollController,
                        padding:
                            const EdgeInsets.all(AppConstant.paddingNormal),
                        itemCount: controller.isFirstLoading.isTrue
                            ? 10
                            : controller.listUser.length,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: AppConstant.paddingNormal,
                          mainAxisSpacing: AppConstant.paddingNormal,
                        ),
                        itemBuilder: (context, index) {
                          if (controller.isFirstLoading.isTrue) {
                            return SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: Get.height,
                                  width: double.infinity,
                                  borderRadius: BorderRadius.circular(8)),
                            );
                          } else {
                            return GestureDetector(
                              onTap: () => Get.toNamed(UserDetailView.pageName,
                                  arguments: controller.listUser[index].id),
                              child: Container(
                                clipBehavior: Clip.hardEdge,
                                decoration: BoxDecoration(
                                  color: AppColors.cBgWhite,
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(
                                          AppConstant.radiusNormal)),
                                  boxShadow: [AppConstant.shadow],
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: Image.network(
                                        controller.listUser[index].picture ??
                                            '',
                                        fit: BoxFit.cover,
                                        width: Get.width,
                                      ),
                                    ),
                                    Container(
                                      padding: const EdgeInsets.all(
                                          AppConstant.paddingSmall),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            controller.listUser[index].id ?? '',
                                            style: AppText.fCaptionLarge
                                                .copyWith(
                                                    color: AppColors.cGrey60),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          const SizedBox(
                                              height: AppConstant
                                                  .paddingExtraSmall),
                                          Text(
                                            "${controller.listUser[index].firstName ?? ''} ${controller.listUser[index].lastName ?? ''}",
                                            style: AppText.fBodyLarge.copyWith(
                                                fontWeight: FontWeight.bold),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }
                        },
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
