import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:skeletons/skeletons.dart';
import 'package:techtest_logique/app/modules/widgets/label_text_widget.dart';
import 'package:techtest_logique/app/modules/widgets/post_widget.dart';

import '../../../../app_colors.dart';
import '../../../../app_constant.dart';
import '../../../../app_text.dart';
import '../controllers/user_detail_controller.dart';

class UserDetailView extends GetView<UserDetailController> {
  static const pageName = '/user-detail';
  const UserDetailView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leadingWidth: AppConstant.iconSizeLarge,
        title: Container(
          decoration: const BoxDecoration(
              color: AppColors.cGrey20,
              borderRadius:
                  BorderRadius.all(Radius.circular(AppConstant.radiusSmall))),
          child: TextFormField(
            style: AppText.fBodyLarge,
            controller: controller.searchController,
            textInputAction: TextInputAction.search,
            onFieldSubmitted: (value) {},
            textAlign: TextAlign.start,
            textAlignVertical: TextAlignVertical.center,
            decoration: const InputDecoration(
              hintText: "Search in name posts",
              prefixIcon: Icon(Icons.search),
              fillColor: AppColors.cGrey10,
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
              focusColor: AppColors.cAccentError,
            ),
          ),
        ),
        elevation: 1,
        backgroundColor: AppColors.cBgWhite,
      ),
      body: Obx(
        () => controller.isLoading.isTrue
            ? const Center(child: CircularProgressIndicator())
            : ListView(
                padding: const EdgeInsets.all(AppConstant.paddingNormal),
                children: [
                  Row(
                    children: [
                      Container(
                        height: AppConstant.profileSize,
                        width: AppConstant.profileSize,
                        decoration: const BoxDecoration(
                            color: AppColors.cGrey40Disable,
                            borderRadius:
                                BorderRadius.all(Radius.circular(999))),
                        clipBehavior: Clip.hardEdge,
                        child: controller.isLoading.isFalse
                            ? Image.network(
                                controller.dataUser.value?.picture ?? "",
                                fit: BoxFit.cover,
                              )
                            : Container(),
                      ),
                    ],
                  ),
                  const SizedBox(height: AppConstant.paddingSmall),
                  Text(
                    "${controller.dataUser.value?.firstName ?? ''} ${controller.dataUser.value?.lastName ?? ''}",
                    style:
                        AppText.fHeading3.copyWith(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: AppConstant.paddingSmall),
                  labelText(FontAwesomeIcons.solidEnvelope,
                      controller.dataUser.value?.email ?? ''),
                  const SizedBox(height: AppConstant.paddingSmall),
                  labelText(FontAwesomeIcons.locationDot,
                      "${controller.dataUser.value?.location?.street}, ${controller.dataUser.value?.location?.city}, ${controller.dataUser.value?.location?.country}"),
                  const SizedBox(height: AppConstant.paddingNormal),

                  //post
                  ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: controller.isPostLoading.isTrue
                        ? 10
                        : controller.listPost.length,
                    itemBuilder: (context, index) {
                      if (controller.isPostLoading.isTrue) {
                        return SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 100,
                              width: double.infinity,
                              borderRadius: BorderRadius.circular(8)),
                        );
                      } else {
                        return postWidget(controller.listPost[index]);
                      }
                    },
                    separatorBuilder: (context, index) {
                      return const SizedBox(height: AppConstant.paddingNormal);
                    },
                  )
                ],
              ),
      ),
    );
  }
}
