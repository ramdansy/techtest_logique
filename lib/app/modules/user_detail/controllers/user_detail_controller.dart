import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:techtest_logique/data/entities/post_preview.dart';
import 'package:techtest_logique/data/entities/user.dart';
import 'package:techtest_logique/domain/usecases/post/get_post_by_yser_id.dart';
import 'package:techtest_logique/domain/usecases/users/get_user_by_id.dart';

class UserDetailController extends GetxController {
  late final GetUserById getUserById;
  late final GetPostByUserId getPostByUserId;
  UserDetailController(this.getUserById, this.getPostByUserId);

  final searchController = TextEditingController();
  RxBool isLoading = false.obs, isPostLoading = false.obs;
  Rxn<User?> dataUser = Rxn<User?>();
  RxList<PostPreview> listPost = <PostPreview>[].obs;

  @override
  void onReady() {
    super.onReady();
    fetchDataUser();
  }

  void fetchDataUser() async {
    isLoading.value = true;

    final resUser = await getUserById.execute(Get.arguments);
    resUser.fold(
      (failure) => Fluttertoast.showToast(msg: failure.message),
      (data) {
        dataUser.value = data;
      },
    );

    isLoading.value = false;
    fetchListPost();
  }

  void fetchListPost() async {
    isPostLoading.value = true;

    final resPostByUser =
        await getPostByUserId.execute(dataUser.value?.id ?? "", 0, 20);
    resPostByUser.fold(
      (failure) => Fluttertoast.showToast(msg: failure.message),
      (data) {
        listPost.addAll(data.data ?? []);
      },
    );

    isPostLoading.value = false;
  }
}
