import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:techtest_logique/app_colors.dart';
import 'package:techtest_logique/app_constant.dart';
import 'package:techtest_logique/app_text.dart';

import '../controllers/likes_controller.dart';

class LikesView extends GetView<LikesController> {
  static const pageName = '/likes';
  const LikesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: AppColors.cBgWhite,
          width: Get.width,
          height: Get.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(AppConstant.paddingNormal),
                child: Text(
                  AppConstant.textLikes,
                  style:
                      AppText.fHeading3.copyWith(fontWeight: FontWeight.bold),
                ),
              ),
              Expanded(child: Container()),
            ],
          ),
        ),
      ),
    );
  }
}
