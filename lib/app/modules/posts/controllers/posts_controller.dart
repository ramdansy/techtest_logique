import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:techtest_logique/data/entities/post_preview.dart';
import 'package:techtest_logique/domain/usecases/post/get_all_post.dart';

class PostsController extends GetxController {
  late final GetAllPost getAllPost;

  PostsController(this.getAllPost);

  RxBool isLoading = false.obs;
  RxList<PostPreview> listPost = <PostPreview>[].obs;

  @override
  void onReady() {
    super.onReady();
    fetchAllPost();
  }

  Future<void> fetchAllPost() async {
    isLoading.value = true;

    final resAllPost = await getAllPost.execute(0, 10);
    resAllPost.fold(
      (failure) => Fluttertoast.showToast(msg: failure.message),
      (data) {
        listPost.addAll(data.data ?? []);
      },
    );

    isLoading.value = false;
  }
}
