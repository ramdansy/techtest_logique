import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:skeletons/skeletons.dart';
import 'package:techtest_logique/app/modules/widgets/post_widget.dart';
import 'package:techtest_logique/app_colors.dart';
import 'package:techtest_logique/app_constant.dart';
import 'package:techtest_logique/app_text.dart';

import '../controllers/posts_controller.dart';

class PostsView extends GetView<PostsController> {
  static const pageName = '/posts';
  const PostsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: AppColors.cBgWhite,
          width: Get.width,
          height: Get.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(AppConstant.paddingNormal),
                child: Text(
                  AppConstant.textPost,
                  style:
                      AppText.fHeading3.copyWith(fontWeight: FontWeight.bold),
                ),
              ),
              Expanded(
                child: Obx(() => RefreshIndicator(
                      onRefresh: () => controller.fetchAllPost(),
                      child: ListView.separated(
                        padding:
                            const EdgeInsets.all(AppConstant.paddingNormal),
                        itemCount: controller.isLoading.isTrue
                            ? 10
                            : controller.listPost.length,
                        itemBuilder: (context, index) {
                          if (controller.isLoading.isTrue) {
                            return SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: 200,
                                  width: double.infinity,
                                  borderRadius: BorderRadius.circular(8)),
                            );
                          }
                          return postWidget(controller.listPost[index]);
                        },
                        separatorBuilder: (context, index) {
                          return const SizedBox(
                              height: AppConstant.paddingNormal);
                        },
                      ),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
