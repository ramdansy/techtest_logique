import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:techtest_logique/app_constant.dart';
import 'package:techtest_logique/app_text.dart';

Widget labelText(IconData icon, String text) {
  return Row(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      SizedBox(
        width: AppConstant.iconSizeNormal,
        child: FaIcon(
          icon,
          size: AppConstant.iconSizeNormal,
        ),
      ),
      const SizedBox(width: AppConstant.paddingSmall),
      Expanded(
        child: Text(
          text,
          style: AppText.fBodyLarge,
        ),
      ),
    ],
  );
}
