import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:techtest_logique/app/modules/widgets/label_text_widget.dart';
import 'package:techtest_logique/app_colors.dart';
import 'package:techtest_logique/app_constant.dart';
import 'package:techtest_logique/app_text.dart';
import 'package:techtest_logique/data/entities/post_preview.dart';

Widget postWidget(PostPreview post) {
  return Container(
    padding: const EdgeInsets.all(AppConstant.paddingNormal),
    decoration: BoxDecoration(
        color: AppColors.cGrey10,
        boxShadow: [AppConstant.shadow],
        borderRadius: const BorderRadius.all(
          Radius.circular(AppConstant.radiusNormal),
        )),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: AppConstant.iconSizeNormal,
              width: AppConstant.iconSizeNormal,
              child: CircleAvatar(
                backgroundImage: NetworkImage(
                    post.owner?.picture ?? ""),
              ),
            ),
            const SizedBox(width: AppConstant.paddingNormal),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${post.owner?.firstName ?? ''} ${post.owner?.lastName ?? ''}",
                  style: AppText.fBodyLarge,
                ),
                Text(
                  "${post.publishDate}",
                  style: AppText.fCaptionLarge,
                ),
              ],
            )
          ],
        ),
        const SizedBox(height: AppConstant.paddingNormal),
        SizedBox(
          width: Get.width,
          height: 200,
          child: Image.network(
            post.image ?? "",
            fit: BoxFit.cover,
          ),
        ),
        const SizedBox(height: AppConstant.paddingNormal),
        Row(
          children: (post.tags ?? []).map((tag) {
            return Container(
              padding: const EdgeInsets.symmetric(
                vertical: AppConstant.paddingSmall,
                horizontal: AppConstant.paddingNormal,
              ),
              margin: const EdgeInsets.only(right: AppConstant.paddingSmall),
              decoration: const BoxDecoration(
                  color: AppColors.cPrimary10,
                  borderRadius: BorderRadius.all(Radius.circular(99))),
              child: Text(
                tag,
                style: AppText.fBodySmall.copyWith(
                    color: AppColors.cPrimary70, fontWeight: FontWeight.w600),
              ),
            );
          }).toList(),
        ),
        const SizedBox(height: AppConstant.paddingNormal),
        Text(
          post.text ?? "",
          style: AppText.fBodySmall,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
        ),
        const SizedBox(height: AppConstant.paddingNormal),
        labelText(FontAwesomeIcons.solidHeart,
            (post.likes ?? 0).toString()),
      ],
    ),
  );
}
