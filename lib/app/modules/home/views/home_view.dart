import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:get/get.dart';
import 'package:techtest_logique/app/modules/users/views/users_view.dart';
import 'package:techtest_logique/app_colors.dart';
import 'package:techtest_logique/app_constant.dart';
import 'package:techtest_logique/app_text.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.cBgWhite,
      extendBody: true,
      body: Navigator(
        key: Get.nestedKey(controller.getNavigatorId()),
        initialRoute: UsersView.pageName,
        onGenerateRoute: controller.onGenerateRoute,
      ),
      bottomNavigationBar: Obx(
        () => Container(
          color: AppColors.cBgWhite,
          height: AppConstant.bottomNavHeight,
          child: BottomNavigationBar(
            backgroundColor: AppColors.cBgWhite,
            elevation: 2,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            onTap: controller.changePage,
            currentIndex: controller.currentIndex.value,
            type: BottomNavigationBarType.fixed,
            unselectedItemColor: AppColors.cPrimary10,
            selectedItemColor: AppColors.cPrimary,
            unselectedLabelStyle: AppText.fCaptionLarge
                .copyWith(fontWeight: FontWeight.bold, height: 1.5),
            selectedLabelStyle: AppText.fCaptionLarge
                .copyWith(fontWeight: FontWeight.bold, height: 1.5),
            iconSize: AppConstant.iconSizeNormal,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: FaIcon(FontAwesomeIcons.solidUser),
                label: "",
              ),
              BottomNavigationBarItem(
                icon: FaIcon(FontAwesomeIcons.clipboardList),
                label: "",
              ),
              BottomNavigationBarItem(
                icon: FaIcon(FontAwesomeIcons.solidHeart),
                label: "",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
