import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:techtest_logique/app/modules/likes/bindings/likes_binding.dart';
import 'package:techtest_logique/app/modules/likes/views/likes_view.dart';
import 'package:techtest_logique/app/modules/posts/bindings/posts_binding.dart';
import 'package:techtest_logique/app/modules/posts/views/posts_view.dart';
import 'package:techtest_logique/app/modules/users/bindings/users_binding.dart';
import 'package:techtest_logique/app/modules/users/views/users_view.dart';

class HomeController extends GetxController {
  RxInt currentIndex = 0.obs;

  final pages = <String>[
    UsersView.pageName,
    PostsView.pageName,
    LikesView.pageName,
  ];

  int getNavigatorId() {
    return 1;
  }

  void changePage(int index) {
    currentIndex.value = index;
    Get.toNamed(pages[index], id: 1);
  }

  Route? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case UsersView.pageName:
        return GetPageRoute(
          settings: settings,
          page: () => const UsersView(),
          binding: UsersBinding(),
          transition: Transition.noTransition,
        );
      case PostsView.pageName:
        return GetPageRoute(
          settings: settings,
          page: () => const PostsView(),
          binding: PostsBinding(),
          transition: Transition.noTransition,
        );
      case LikesView.pageName:
        return GetPageRoute(
          settings: settings,
          page: () => const LikesView(),
          binding: LikesBinding(),
          transition: Transition.noTransition,
        );
      default:
        return null;
    }
  }
}
