// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const USERS = _Paths.USERS;
  static const POSTS = _Paths.POSTS;
  static const LIKES = _Paths.LIKES;
  static const USER_DETAIL = _Paths.USER_DETAIL;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const USERS = '/users';
  static const POSTS = '/posts';
  static const LIKES = '/likes';
  static const USER_DETAIL = '/user-detail';
}
