import 'package:flutter/material.dart';
import 'package:techtest_logique/app_colors.dart';

class AppConstant {
  static const String baseUrl = "https://dummyapi.io/data/v1";
  static const String appId = "652409095c606b2700cef077";

  //sizing
  static const double bottomNavHeight = 60.0;
  static const double profileSize = 120.0;
  static const double iconSizeSmall = 20.0;
  static const double iconSizeNormal = 24.0;
  static const double iconSizeLarge = 32.0;
  static const double iconSizeExtraLarge = 40.0;

  //padding
  static const double paddingExtraSmall = 4.0;
  static const double paddingSmall = 8.0;
  static const double paddingNormal = 16.0;
  static const double paddingLarge = 24.0;
  static const double paddingExtraLarge = 40.0;

  //radius
  static const double radiusExtraSmall = 4.0;
  static const double radiusSmall = 8.0;
  static const double radiusNormal = 12.0;
  static const double radiusLarge = 16.0;
  static const double radiusExtraLarge = 24.0;

  //text
  static const String textUsers = "Users";
  static const String textPost = "Posts";
  static const String textLikes = "Likes";
  static const String textErrorConnection = "No Internet Connection";

  //shadow
  static BoxShadow shadow = const BoxShadow(
      blurRadius: 1,
      color: AppColors.cGrey40Disable,
      offset: Offset(0, 1),
      spreadRadius: 0);
}
